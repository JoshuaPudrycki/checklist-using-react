Author: Joshua Pudrycki

Purpose:  To introduce myself to React framework.  
Project:  Creates a functional checklist that can be ran in browser.

How to:
1) Open command line (install npm in necessary).
2) Navigate to 'my-app' and rum npm start.
3) Wait for application to open in browser tab. 