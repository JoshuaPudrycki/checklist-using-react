import React, { Component }from 'react';
import './App.css';
import Todos from './components/Todos';

class App extends Component {
	state = {
		todos: [
		{
			id: 1,
			title: 'Learn React',
			completed: true
		},
		{
			id: 2,
			title: 'Learn Azure',
			completed: false
		},
		{
			id: 3,
			title: 'Make dinner',
			completed: false
		}
	]
}
//toggles complete
	markComplete = (id) => {
		this.setState({ todos: this.state.todos.map(todo => {
			if(todo.id===id){
				todo.completed = !todo.completed
		}
			return todo;
		})});
	}
	
	render(){
		console.log(this.state.todos)
	  return (
		<div className="App">
			<Todos todos={this.state.todos} markComplete={this.markComplete} />
			
		</div>
	  );
	}
}

export default App;
